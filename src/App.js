import logo from './images/eiffel.jpg';
import pisa from './images/pisa.jpg';
import bolonia from './images/bologna-5082781_1280.jpg';
import colpatria from './images/bogota-221355_1280.jpg';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import EiffelPage from "./components/EiffelPage";
import PisaPage from "./components/PisaPage";
import BoloniaPage from "./components/BoloniaPage";
import ColpatriaPage from "./components/ColpatriaPage";

function App() {
    return (
        <Router>
            <div className="App">
                <Routes>
                    <Route path="/" element={<HomePage />} />
                    <Route path="/eiffel" element={<EiffelPage />} />
                    <Route path="/pisa" element={<PisaPage />} />
                    <Route path="/bolonia" element={<BoloniaPage />} />
                    <Route path="/colpatria" element={<ColpatriaPage />} />
                </Routes>
            </div>
        </Router>
    );
}
const HomePage = () => (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <h1>Torre Eifel</h1>
                        <p>Situado a 115 metros por encima del suelo, posee una superficie de 1650 metros cuadrados
                            aproximadamente; puede soportar la presencia simultánea de alrededor de 1600 personas.</p>
                        <ul className="actions stacked">
                            <li><a href="/eiffel" className="button large wide smooth-scroll-middle">Conocer más</a></li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={logo} alt="Eifel"/>
                    </div>
                </section>
                <section
                    className="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in"
                    id="first">
                    <div className="content">
                        <h2>Torreo de Pisa</h2>
                        <p>
                            La torre de Pisa o torre inclinada de Pisa es la torre campanario de la catedral de Pisa,
                            situada en la plaza del Duomo de Pisa, en la ciudad del mismo nombre, municipio de la región
                            italiana de la Toscana y capital de la provincia homónima de Italia.
                        </p>
                        <ul className="actions stacked">
                            <li><a href="/pisa" className="button large wide smooth-scroll-middle">Conocer más</a></li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={pisa} alt="Pisa"/>
                    </div>
                </section>


                <section
                    className="spotlight style1 orient-left content-align-left image-position-center onscroll-image-fade-in">
                    <div className="content">
                        <h2>Torres de Bolonia</h2>
                        <p>Se ha debatido mucho sobre el número de torres que de seguro atestaban Bolonia en la Edad
                            Media, antes de las sucesivas destrucciones o derrumbamientos de muchas de ellas. El conde
                            Giovanni Gozzadini, senador del Reino de Italia, fue la primera persona en ocuparse de
                            estudiar la historia de la ciudad. Estudiando los archivos ciudadanos relativos a los
                            documentos de compra venta, llegó a la conclusión de que había llegado a haber no menos de
                            210 torres, una cifra verdaderamente descomunal, considerando el tamaño de la Bolonia
                            medieval.</p>
                        <ul className="actions stacked">
                            <li><a href="/bolonia" className="button large wide smooth-scroll-middle">Conocer más</a></li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={bolonia} alt="Bolonia"/>
                    </div>
                </section>
                <section
                    className="spotlight style1 orient-right content-align-left image-position-center onscroll-image-fade-in">
                    <div className="content">
                        <h2>Torre Colpatria</h2>
                        <p>
                            La Torre Colpatria es un rascacielos situado en Bogotá, en la zona norte del barrio Las
                            Nieves. Su zona comercial constituye una prolongación del Centro Internacional de la ciudad.
                            Con sus 50 pisos, es el tercer edificio más alto de la ciudad así como uno de sus iconos.
                            Fue terminada en 1978, y tiene una altura de 196 m.2​ En ella se encuentran las oficinas de
                            diversas compañías, entre las cuales están las empresas del Grupo Colpatria, propietario de
                            la torre.
                        </p>
                        <ul className="actions stacked">
                            <li><a href="/colpatria" className="button large wide smooth-scroll-middle">Conocer más</a></li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={colpatria} alt="Colpatria"/>
                    </div>
                </section>
                <section className="wrapper style1 align-center">
                    <div className="inner">
                        <h2>La importancia de las torres en la historia de la humanidad</h2>
                        <p>
                            Las torres han desempeñado un papel crucial en la historia de la construcción, simbolizando
                            poder, protección y avance tecnológico. Desde las antiguas torres de vigilancia y las
                            fortificaciones medievales hasta los modernos rascacielos, estas estructuras han demostrado
                            la capacidad humana para desafiar los límites arquitectónicos y alcanzar nuevas alturas. Las
                            torres, como la Torre de Babel o la Torre Eiffel, no solo han servido propósitos prácticos
                            como la defensa y la comunicación, sino que también han representado hitos culturales y de
                            ingeniería, reflejando la evolución de las técnicas constructivas y el ingenio creativo a lo
                            largo de los siglos.
                        </p></div>

                </section>
                <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
    );
export default App;
