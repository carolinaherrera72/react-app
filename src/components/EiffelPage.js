import React from 'react';
import eifel from "../images/tower-103417_1280.jpg";


const EiffelPage = () => {
    return (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <h1>Torre Eifel</h1>
                        <p>La Torre Eiffel, uno de los monumentos más icónicos del mundo, fue construida para la
                            Exposición Universal de 1889 en París, con motivo del centenario de la Revolución Francesa.
                            Diseñada por el ingeniero Gustave Eiffel, la torre se erigió entre 1887 y 1889, utilizando
                            más de 18,000 piezas de hierro y 2.5 millones de remaches. Originalmente, la torre fue
                            objeto de controversia y críticas por parte de algunos artistas y escritores de la época,
                            quienes la consideraban una monstruosidad arquitectónica. Sin embargo, con el tiempo, la
                            Torre Eiffel se convirtió en un símbolo de innovación y el ingenio humano, así como un
                            emblema cultural de Francia. Hoy en día, atrae a millones de visitantes de todo el mundo,
                            siendo una de las atracciones turísticas más populares del planeta.</p>
                        <ul className="actions stacked">
                            <li><a href="/" className="button large wide smooth-scroll-middle">Volver</a>
                            </li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={eifel} alt=""/>
                    </div>
                </section>

                <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
    );
}

export default EiffelPage;