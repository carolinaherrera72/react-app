import React from 'react';
import bolonia from "../images/bologna-2630367_1280.jpg";


const BoloniaPage = () => {
    return (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <h1>Torres de Bologna</h1>
                        <p>
                            Las Torres de Bolonia, ubicadas en la ciudad italiana de Bolonia, son estructuras
                            emblemáticas que datan de la Edad Media. Durante el siglo XII, Bolonia contaba con un
                            impresionante número de torres, algunas fuentes sugieren que llegaron a existir más de 100.
                            Estas torres servían tanto como estructuras defensivas como símbolos de poder y riqueza de
                            las familias nobles que las construían.

                            Las dos torres más famosas y mejor conservadas de Bolonia son la Torre degli Asinelli y la
                            Torre Garisenda. La Torre degli Asinelli, con sus 97 metros de altura, es la torre medieval
                            inclinada más alta del mundo, y ofrece vistas espectaculares de la ciudad. A su lado se
                            encuentra la Torre Garisenda, más baja, pero notable por su inclinación pronunciada que
                            alcanzó tal punto que tuvo que ser recortada en la Edad Media por razones de seguridad.

                            Las torres de Bolonia representan un testimonio fascinante de la arquitectura y la vida
                            urbana medieval, así como del dinamismo económico y social de la ciudad durante esa época.
                            Hoy en día, son un importante atractivo turístico y un símbolo histórico de Bolonia.
                        </p>
                        <ul className="actions stacked">
                            <li><a href="/" className="button large wide smooth-scroll-middle">Volver</a>
                            </li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={bolonia} alt=""/>
                    </div>
                </section>

                <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
    );
}

export default BoloniaPage;