import React from 'react';
import pisa1 from "../images/pisa-771404_1280.jpg";


const PisaPage = () => {
    return (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <h1>Torre de Pisa</h1>
                        <p>
                            La Torre de Pisa, conocida mundialmente por su pronunciada inclinación, es el campanario de
                            la catedral de Pisa, ubicada en la Piazza del Duomo en la ciudad de Pisa, Italia. Su
                            construcción comenzó en 1173 y se prolongó durante casi 200 años debido a interrupciones
                            causadas por guerras y otros conflictos.

                            Desde el inicio de su construcción, la torre comenzó a inclinarse debido al asentamiento del
                            terreno inestable sobre el que se construía. Este problema no se pudo corregir
                            completamente, lo que resultó en su característica inclinación. La torre tiene ocho niveles,
                            incluyendo la cámara de las campanas, y alcanza una altura de 56 metros en el lado más alto
                            y aproximadamente 55 metros en el lado más bajo.

                            A lo largo de los siglos, se han realizado numerosos esfuerzos para estabilizar la torre y
                            prevenir su colapso. En la década de 1990, se llevaron a cabo trabajos de ingeniería
                            significativos que redujeron su inclinación de 5.5 grados a unos 3.97 grados, asegurando su
                            estabilidad para las futuras generaciones.

                            Hoy en día, la Torre de Pisa es una de las atracciones turísticas más populares del mundo,
                            famosa no solo por su inclinación sino también por su belleza arquitectónica y su historia
                            única.
                        </p>
                        <ul className="actions stacked">
                            <li><a href="/" className="button large wide smooth-scroll-middle">Volver</a>
                            </li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={pisa1} alt="Pisa"/>
                    </div>
                </section>

                <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
    );
}

export default PisaPage;