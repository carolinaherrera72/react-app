import React from 'react';
import colpatria from "../images/bogota-221355_1280.jpg";


const ColpatriaPage = () => {
    return (
        <div className="App">
            <div id="wrapper" className="divided">
                <section
                    className="banner style1 orient-left content-align-left image-position-right fullscreen onload-image-fade-in onload-content-fade-right">
                    <div className="content">
                        <h1>Torre Colpatria</h1>
                        <p>
                            La Torre Colpatria, ubicada en Bogotá, Colombia, es uno de los rascacielos más emblemáticos
                            de la ciudad. Inaugurada en 1979, la torre se construyó como parte del desarrollo del Centro
                            Internacional de Bogotá y rápidamente se convirtió en un símbolo de modernidad y progreso.
                            Con una altura de 196 metros y 50 pisos, fue durante muchos años el edificio más alto de
                            Colombia.

                            Diseñada por la firma de arquitectos Obregón y Valenzuela, la Torre Colpatria se distingue
                            por su esbelta estructura y su notable iluminación nocturna, que ha sido actualizada con
                            luces LED programables que la convierten en un atractivo visual para residentes y turistas.

                            El edificio alberga oficinas de diversas empresas, incluyendo las del Grupo Colpatria, y su
                            mirador en el último piso ofrece una vista panorámica impresionante de la ciudad de Bogotá.
                            La Torre Colpatria no solo representa un importante centro de negocios, sino también un
                            punto de referencia cultural y turístico en la capital colombiana.
                        </p>
                        <ul className="actions stacked">
                            <li><a href="/" className="button large wide smooth-scroll-middle">Volver</a>
                            </li>
                        </ul>
                    </div>
                    <div className="image">
                        <img src={colpatria} alt="Torre colpatria"/>
                    </div>
                </section>

                <footer className="wrapper style1 align-center">
                    <div className="inner">
                        <p>&copy; Carolina Herrera - Unir 2024</p>
                    </div>
                </footer>
            </div>
        </div>
    );
}

export default ColpatriaPage;